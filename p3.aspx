﻿<%@ page language="C#" masterpagefile="~/MasterPage.master" autoeventwireup="true" inherits="p3, App_Web_-jfpmlsa" title="Untitled Page" %>

<%-- Add content controls here --%><asp:Content ID="Content1" runat="server" 
    contentplaceholderid="ContentPlaceHolder1">

  <h1 align="center" style="color: #7EA31C">
        Micro Controller based Burner Automation
        <br /></h1>
    
</asp:Content>
<asp:Content ID="Content2" runat="server" 
    contentplaceholderid="ContentPlaceHolder2">
<div class="view">
    <p class="text">
        The 
        factory contains an 'acid regeneration plant', where Spend Acid is regenerated 
        to Hydro Chloric Acid, which is the desired output of the plant. For this 
        regeneration of the acid a burner control system is required. The temperature 
        range of the burner is between 9000c -12000c.The reactants required for 
        regeneration plant are Iron Chloride and Spend Acid vapourises and breaks down 
        into H2O and Acid. Iron Chloride also breaks down into Iron Oxide and Chlorine.
       
    </p>
    <p class="text">
       The Chlorine reacts with the water to produce Hydro Chloric Acid 
        (HCL). Here the Spend Acid is regenerated for further use in the plant. After 
        the splitting of the molecules at high temperature it is allowed to cool down at 
        1050c.The cooled gas is then allowed to pass through water. But H2O molecules 
        get vaporized. The Iron molecules in the substrates becomes solid and can be 
        easily separated .The condition of the HCL Acid does not change and get 
        dissolves in water to form the Spend Acid again.</p> <br />
   <h3 style="color: #7EA31C">
     Operation</h3> <br /> 
    <p class="text">
        In 
        our project 'MC Based Burner Automation' first we have to check and control the 
        starting condition, ie to ignite the burner. To ignite the burner we have to 
        check the following condition.</p> 
    <p class="text">
        1. 
        Check the pressure from the LPG.<br />
        2. Check the air flow to the burner.<br />
        3. Some necessary parameters etc. 
    </p>
    <p class="text"> 
        If 
        the flame is OK then the process starts and within minutes the burner gains its 
        temperature range of 9000C-12000c</p>
    <p class="text"> 
       Here 
        first we have to check the LPG preassure and airflow in the combustion tube, to 
        flame the burner. If both are OK, the system is set 'silent' for one minute. If 
        both the LPG preassures and airflow is not OK, the process will be halted. To 
        flame and continous working of burner, we have to use two LPG's, Pilot LPG and 
        main LPG. The Pilot LPG is ON first. If both the preassure and airflow are OK, 
        the pilot LPG and ignition are turn ON for 10 seconds. This 10 sec time is for 
        proper starting of 'flame' in the burner.</p>
    <p class="text">
       In  
        any case the flame is absent, the system will be halted. If flame is OK, the 
        Pilot LPG will close. Before closing the valve, the main LPG valve is opened for 
        supplying the fuel. This burner is in working condition. After that the process 
        explained at introduction takes place in 'Acid Regeneration Plant'.</p>
    <p class="text"> 
       Here 
        in the circuit, we utilize AT89c52 mc, which is similar to at 89c51, except in 
        the 8kb internal flash memory. This flash memory helps to make use of program 
        easily and is double in length than the 9c51. Also a LCD is used for displaying 
        the parameters like LPG preassure, Main LPG ON, air flow, flame etc. The 
        programming is done in 'Embedded c' version. Also relays are used in circuit, 
        which helps to control the various devices.</p>
</div>
</asp:Content>


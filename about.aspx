﻿<%@ page language="C#" masterpagefile="~/MasterPage.master" autoeventwireup="true" inherits="Default2, App_Web_-jfpmlsa" title="Untitled Page" %>

<%-- Add content controls here --%><asp:Content ID="Content1" runat="server" 
    contentplaceholderid="ContentPlaceHolder1">

    <div  class="view"><asp:Label Font-Bold="True" Font-Size="Large" ForeColor="#006600" 
            runat="server"> 
        Project Ideas</asp:Label></div>
        
           
        
       
  
    <div  class="view">
        <p style="font-size" class="text" align="center">
             Its time to submit your computer and information technology project abstracts. 
             Your search for good computer and IT project ideas ends here. This site provides 
             project ideas and abstracts with world class quality. Many a times students face 
             difficulty in understanding the technology behind a project.</p>
    </div>
    <p>
        <br />
    </p>
    <p>
    </p>

</asp:Content>
<asp:Content ID="Content2" runat="server" 
    contentplaceholderid="ContentPlaceHolder2">

    <div id="blocks"  class="view">
        <div class="inside">
            <div class="block1">
                <h3>
                    Beginner projects</h3>
                <span>Small projects for 3 persons or less with basic knowledge. </span>
                <a href="beginner.aspx">
                <img src="images/more.gif" alt="" width="70" height="17" /></a>
            </div>
            <div class="block2">
                <h3>
                    Intermediate projects</h3>
                <span>Mediam projects for 4-6 persons with good knowledge. </span>
                <a href="intermediate.aspx">
                <img src="images/more.gif" alt="" width="70" height="17" /></a>
            </div>
            <div class="block3">
                <h3>
                    Advanced projects 
                </h3>
                <span>Large project for more than 6&nbsp; profissional&nbsp; and expert persons 
                </span><a href="advanced.aspx">
                <img src="images/more.gif" alt="" width="70" height="17" /></a>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" runat="server" 
    contentplaceholderid="ContentPlaceHolder3">
<div  class="view">
    <p  class="text" align="center"><font color="black">
        Our collection of ideas and abstracts are compiled by experts in the industry. 
        Every day more and more projects are being added into the system. A dedicated 
        team of working professionals are working their free time to make this site 
        useful for the students searching for computer and IT projects for their final 
        year academic project. We update the projects every week so that we capture the 
        latest technology trends in the market and create projects based on that.</font></p>
    <br />
    <p class="text" align="center" style="font-size: medium">www.id34s.co.cc © All Rights Reserved</p>
</div>
</asp:Content>


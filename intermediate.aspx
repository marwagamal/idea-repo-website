﻿<%@ page language="C#" masterpagefile="~/MasterPage.master" autoeventwireup="true" inherits="intermediate, App_Web_-jfpmlsa" title="Untitled Page" %>

<%@ Register src="share.ascx" tagname="share" tagprefix="uc1" %>

<%-- Add content controls here --%><asp:Content ID="Content1" runat="server" 
    contentplaceholderid="ContentPlaceHolder1">

    <center class="view"  ><h1 align="center" style="color: #7EA31C" >
        Intermediate Projects
       
</h1>
      <div class="right"> <uc1:share ID="share1" runat="server" /> </div> 
    </center>
</asp:Content>
<asp:Content ID="Content2" runat="server" 
    contentplaceholderid="ContentPlaceHolder2">

    
        <br />
      <br /> 
     <div class="column" align="left" style="padding-left: 100px; margin-left: 30px">
   <ul>
           
           
         <li>
        
         <a href="http://akira.ruc.dk/~mir/plis/studentprojects/35/project.html" >
             A translator from graphical to prolog  </a>   
<br />A translator from graphical component-based embedded systems designs to prolog   <br /> <br /> <br /></li>
       
       
        <li>
       <a href="http://www.iprojectideas.com/?cid=1&pid=75&topic=Abrasion%20Hologram%20Printer" >
            Abrasion Hologram Printer    </a>    
    <br />
            A complete system for printing 3D objects that appear to rotate based on the 
            viewing angle. The resultant holographic image is printed with a five-axis 
            mechanical device that scratches extremely fine arcs on a plastic plane. 
    <br /> <br /> <br /> </li> <li>
      
      
       <a href="http://www.iprojectideas.com/?cid=1&pid=76&topic=Car%20to%20Cell%20Phone" >
             Car to Cell Phone    </a>   
    <br />
             Allowing car owners to interface with their automobile alarm systems with a 
             handheld device, such as a cell phone. <br /> <br /> <br />  </li>
 
     <li>
      
       <a href="http://www.iprojectideas.com/?cid=1&pid=77&topic=Smart%20House%20Network" >
         Smart House Network   </a>    
<br />
         Allowing homeowners to remotely access a home network and change the state of 
         attached lights, door locks and appliances. <br /> <br /> <br />  </li>
<li>

       <a href="http://www.iprojectideas.com/?cid=1&pid=86&topic=Broadcasting%20with%20minimal%20transmission%20power:" >
        Broadcasting with minimal transmission power:   </a>   
<br />The main purpose of the project is to broadcast a message in the wireless network 
       with the minimal transmission power.  <br /> <br /> <br /></li>
        
           <li>
         <a href="http://www.iprojectideas.com/?cid=1&pid=87&topic=MRS%20chat%20server" >
               MRS chat server  </a>   
<br />This project is about implementing a chat server. The clients have to get connected to 
               the chat server and should specify to which client it needs to get connected. 
               Then the chat server will make those two clients to get connected and the 
               clients can exchange messages. <br /> <br /> <br /></li>
        
        
        
           <li>
         <a href="http://www.iprojectideas.com/?cid=1&pid=88&topic=Home%20Appliance%20and%20Control%20System%20%28HACS%29" >
               Home Appliance and Control System (HACS)   </a>   
<br /> This is an application project, which is useful in controlling household appliances 
               using a mobile phone or a PDA  
               <br />  <br />  <br />  <br />
               <br />www.id34s.co.cc © All Rights Reserved<br /></li>
        
         
        
</ul>
   
                </div>
                
                
                <div class="column" style="padding-left: 50px"><ul>
                
                
                
                     <li>
    <a href="http://www.iprojectideas.com/?cid=1&pid=89&topic=Voice%20Spam%20Protection:" >
                         Voice Spam Protection  </a>
    <br />
                         The purpose of the project is to block spam calls. This is implemented using a 
                         Progressive Multi-Gray leveling algorithm where each user is given a gray level 
                         and based on his current gray level, the calls are either connected or blocked. 
                         This project is implemented in java for the call connection and MySQL is used 
                         for the back end in order to maintain the gray level of the users.
      <br /> <br /> <br /> </li>
     
      
                
                
         
         <li>
         <a href="http://www.iprojectideas.com/?cid=1&pid=90&topic=Termination%20Detection%20in%20a%20Distributed%20Systems:" >
             Termination Detection in a Distributed Systems </a>   
<br />This project implements Huang&#65533;s algorithm to detect the termination among the 
             distributed systems. This project was implemented with C language using socket 
             programming to establish connection among different nodes. The termination of 
             all the nodes will be identified using Huang&#65533;s algorithm. <br /> <br /> <br /></li>
        
        
        
                <li>
         <a href="http://www.iprojectideas.com/?cid=1&pid=91&topic=Autocartoon" >
                    Autocartoon   </a>   
<br />Make a software which generate cartoon using your original photographs. This project can be 
                    used to create avatars. We can further use these avatars in games.  <br /> <br /> <br /></li>
        
        
        
                
                
                   <li>
         <a href="http://it.okstate.edu/itprojects/documents/spambuscase.pdf" >
                       Anti Spam Project  </a>   
<br />: Spam is causing serious problems to our mail boxes.Now days there quantity is even more than 
                       real messages. You can build anti spam project. <br /> <br /> <br /></li>
        
        
        
        
         <li>
         <a href="http://www.iprojectideas.com/?cid=6&pid=33&topic=Virtual%20Lab" >
             Virtual Lab  </a>   
<br />There are labs in our college. We do projects there , but what, if we can attend our 
             labs from comfort of our home or from our laptop sitting in a garden. These 
             files will help you for completing your project.<br /> 
 <a href="http://it.okstate.edu//howto/documents/Virtual_Labs_Windows.pdf" >
             Virtual Labs for Windows - pdf </a><br />
       <a href="http://it.okstate.edu//howto/documents/Virtual_Labs_Mac.pdf" >
             Virtual Labs for Mac - pdf </a><br />
       <a href="http://it.okstate.edu//howto/documents/Virtual_Labs_Linux.pdf" >
             Virtual Labs for Linux - pdf </a><br /> <br /> <br />
    </li>
        
         
                
              
                </ul>  
                </div>
                <br /><br /><br />
               

</asp:Content>




<asp:Content ID="Content3" runat="server" 
    contentplaceholderid="ContentPlaceHolder3">

    <p class="text" align="center" style="font-size: medium">&nbsp;</p>

</asp:Content>






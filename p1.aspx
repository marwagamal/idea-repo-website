﻿<%@ page language="C#" masterpagefile="~/MasterPage.master" autoeventwireup="true" inherits="p1, App_Web_-jfpmlsa" title="Untitled Page" %>

<%-- Add content controls here --%><asp:Content ID="Content1" runat="server" 
    contentplaceholderid="ContentPlaceHolder1">

    
    <h1 align="center" class="inside" style="color: #7EA31C">Network Monitoring System Communication
    </h1>
        <br />
  
   <div class="view">
    
    <h3 style="color: #7EA31C" >
    
    <br /> <br />

About The System <br /> <br /> </h3>
<p class="text">
Network Monitoring System enables the local administrator to perform real time monitoring, data acquisition, data analysis and transmission .Here the administrator's work is made easier and simple.

Administrator is having options for new user registration, updating and deleting user accounts, enable or disable user accounts, time allotment of users, he can view the system details of any user, he can view the hardware and software defects in any system connected through the network, he can capture the system of any user using his system, detect failure in network and can access the full control of any user at any time, he can shutdown and reboot any system, chatting, and some other facilities in this system.

Whenever the administrator finds that a particular user performs an illegal activity, he can disable that user's account and the user is not permitted to log into the system.<br />

The Administrator has some other options to view some details regarding the accounts and the registered users in the network. He can view the complaints, requests and feedback made by the users. He can view the logged users details such as current logic time, last logos time etc., time allotment list of all the users, system details of a particular user in the network etc..Only the registered user can use the service provided by flee System.

The services offered to the user are mailing, chatting, call services such as complain. request and feedback submission, and change password and view user's account details. Security features are also enhanced in the software by checking the user name, password and category. Time allotment for the user is done based on the user id. This system consists of mainly four modules namely failure detection, system details, user details and remote section. <br /> <br /> </p>


<h3 style="color: #7EA31C">
Project description <br /> <br />

</h3>

<p class="text">
The main objective of this project is to develop a fullfledged system giving detailed information about local network of a Campus. This project s focused mainly in administrative task.This software enables the local network administrator to view the entire network structure, to perform client control and real time monitoring, to find LAN traffic and failure detection and he can chat with the clients and check the complaints registered by them. The administrator can also change his password for ensuring security in the network.

Here the administrator monitors all the machines in the network and he has the facility to view the system configuration. He also monitor all the users logged on to the network and has the facility to , view the details of the users such as username, last logoOFFtime, last logintime, login date and privilege.

Administrator can perform real time monitoring by viewing the current connected machines in the network and logged in users and hence can find their processor, application and memory details.<br /> Administrator has the facility to know actual network structure, desktop capture, shutdown, restart and IogOFF a remote system from his sysem.

Administrator can defect the traffic ie. the frequency, delay and bandwidth of the network cable laid and hence he could take a decision whether to replace network cables of higher bandwidth and thereby get the speed of data transfer increased. Security features are also included to the software by checking username and password.

Administrator can send online messages to all remote systems in the network or send online messages to a particular users logged into remote system. Call registration facility is provided for the users to register his complaints and requests. Administrator has the facility to view the complaint registered by the user and send a reply back to the user. The users can also send feedback about the resources provided by tile organization. Administrator can find the software installed in a machine.
    
    
    
    </p>
    
    
    
   </div>

</asp:Content>

